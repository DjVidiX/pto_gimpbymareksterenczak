#include "map_normal.h"

#include "edge_sobel.h"
#include "map_height.h"

MapNormal::MapNormal(PNM* img) :
    Convolution(img)
{
}

MapNormal::MapNormal(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* MapNormal::transform()
{
    int width  = image->width(),
        height = image->height();

    double strength = getParameter("strength").toDouble();

    PNM* newImage = new PNM(width, height, image->format());

    PNM* mapHeightImage = MapHeight(image).transform();

    EdgeSobel edgeSobel(mapHeightImage);

    math::matrix<float>* Gx = edgeSobel.rawHorizontalDetection();
    math::matrix<float>* Gy = edgeSobel.rawVerticalDetection();

    for (int x = 0; x < image->width(); x++) {
        for (int y = 0; y < image->height(); y++) {

            float d_x = (*Gx)(x, y) / 255;
            float d_y = (*Gy)(x, y) / 255;
            float d_z = 1 / strength;

            float length = sqrt(pow(d_x, 2) + pow(d_y, 2) + pow(d_z, 2));

            d_x /= length;
            d_y /= length;
            d_z /= length;

            float norm = 255 / 2.0;

            d_x = (d_x + 1.0) * norm;
            d_y = (d_y + 1.0) * norm;
            d_z = (d_z + 1.0) * norm;

            newImage->setPixel(x,y, qRgb(d_x, d_y, d_z));
        }
    }

    return newImage;
}
