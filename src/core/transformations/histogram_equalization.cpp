#include "histogram_equalization.h"

#include "../histogram.h"

HistogramEqualization::HistogramEqualization(PNM* img) :
    Transformation(img)
{
}

HistogramEqualization::HistogramEqualization(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

float partialSum(float* P, int j) {
    float sum = 0.0;
    for (int i= 0; i<=j; i++) {
        sum += P[i];
    }
    return sum;
}

PNM* HistogramEqualization::transform()
{
    int width = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    if (image->format() == QImage::Format_Indexed8) {

        QHash<int, int> *channel = image->getHistogram()->get(Histogram::LChannel);
        float P[PIXEL_VAL_MAX+1];
        float D[PIXEL_VAL_MAX+1];
        int LUT[PIXEL_VAL_MAX+1];

        float n = width * height;

        for (int i = 0; i < PIXEL_VAL_MAX; i++) {
            P[i] = channel->value(i) / n;
        }

        for (int i = 0; i < PIXEL_VAL_MAX; i++) {
            D[i] = partialSum(P, i);
        }

        for (int i = 0; i < PIXEL_VAL_MAX; i++) {
            LUT[i] = int(D[i]*PIXEL_VAL_MAX);
        }

        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                QRgb pixel = image->pixel(x,y);
                int v = qGray(pixel);
                newImage->setPixel(x,y, LUT[v]);
            }



    } else if (image->format() == QImage::Format_RGB32) {

        QHash<int, int> *channelR = image->getHistogram()->get(Histogram::RChannel);
        QHash<int, int> *channelG = image->getHistogram()->get(Histogram::GChannel);
        QHash<int, int> *channelB = image->getHistogram()->get(Histogram::BChannel);

        float P[3][PIXEL_VAL_MAX];  // 0 -> R, 1 -> G, 2 -> B
        float D[3][PIXEL_VAL_MAX];
        int LUT[3][PIXEL_VAL_MAX];

        float n = width * height;

        for (int i = 0; i < PIXEL_VAL_MAX; i++) {
            P[0][i] = channelR->value(i) / n;
            P[1][i] = channelG->value(i) / n;
            P[2][i] = channelB->value(i) / n;
        }

        for (int i = 0; i < PIXEL_VAL_MAX; i++) {
            D[0][i] = partialSum(P[0], i);
            D[1][i] = partialSum(P[1], i);
            D[2][i] = partialSum(P[2], i);
        }

        for (int i = 0; i < PIXEL_VAL_MAX; i++) {
            LUT[0][i] = int(D[0][i]*PIXEL_VAL_MAX);
            LUT[1][i] = int(D[1][i]*PIXEL_VAL_MAX);
            LUT[2][i] = int(D[2][i]*PIXEL_VAL_MAX);
        }

        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                QRgb pixel = image->pixel(x,y);
                int r = qRed(pixel);
                int g = qGreen(pixel);
                int b = qBlue(pixel);

                newImage->setPixel(x,y, qRgb(LUT[0][r], LUT[1][g], LUT[2][b]));
            }

    }

    return newImage;
}

