#include "bin_gradient.h"
#include "corner_harris.h"
#include "edge_laplacian.h"
#include "hough.h"
#include "hough_rectangles.h"

#include <math.h>       /* acos */

#define PI 3.14159265

HoughRectangles::HoughRectangles(PNM* img) :
    Transformation(img)
{
}

HoughRectangles::HoughRectangles(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

double getVectorLength(QPair<int, int> x) {
    return sqrt(pow(x.first, 2) + pow(x.second, 2));
}

QPair<int, int> getVector(QPair<int, int> x, QPair<int, int> y) {

    return QPair<int, int>(y.first - x.first, y.second - x.second);
}

bool check_rigth_angle(QPair<int, int> common, QPair<int, int> x, QPair<int, int> y) {

    QPair<int, int> x_vec = getVector(common, x);
    QPair<int, int> y_vec = getVector(common, y);

    double iloczyn = x_vec.first * y_vec.first + x_vec.second * y_vec.second;
    double angle_param = iloczyn / (getVectorLength(x_vec) * getVectorLength(y_vec));
    double result = acos (angle_param) * 180.0 / PI;

//    if (result > 89.0 && result < 91.0 ) {
//        return true;
//    }
    // Rozwiązanie nie działa prawidłowo ze względu na nieuniwersalne rysowanie prostokątów

    return x_vec.first*y_vec.first + x_vec.second*y_vec.second == 0;

}

std::vector<QPair<int, int>> get_third_right_angle_points(QPair<int, int> common, QPair<int, int> x, std::vector<QPair<int,int>> points) {

    std::vector<QPair<int, int>> third_points;
    for (unsigned i = 0; i < points.size(); i++) {
        if (points.at(i) == common || points.at(i) == x) continue;

        if (check_rigth_angle(common, x, points.at(i))) {
            third_points.push_back(points.at(i));
        }
    }

    return third_points;
}

std::vector<std::vector<QPair<int, int>>> get_rectagles(QPair<int, int> common, std::vector<QPair<int,int>> points, unsigned index) {

    std::vector<std::vector<QPair<int, int>>> rectangles;

    for (unsigned i = 0; i < points.size(); i++) {

        if (i == index) continue;

        for (unsigned j = 0; j < points.size(); j++) {
            if (j == i || j == index) continue;

            QPair<int,int> x = points.at(i);
            QPair<int,int> y = points.at(j);

            if (check_rigth_angle(common, x, y)) {

                std::vector<QPair<int, int>> x_third_points = get_third_right_angle_points(x, common, points);
                std::vector<QPair<int, int>> y_third_points = get_third_right_angle_points(y, common, points);

                if (x_third_points.size() > 0 && y_third_points.size() > 0) {

                    for (unsigned l = 0; l < x_third_points.size(); l++) {
                        for (unsigned k = 0; k < y_third_points.size(); k++) {
                            if (x_third_points.at(l) == y_third_points.at(k)) {
                                std::vector<QPair<int, int>> rectangle;
                                rectangle.push_back(common);
                                rectangle.push_back(x);
                                rectangle.push_back(y);
                                rectangle.push_back(x_third_points.at(l));

                                rectangles.push_back(rectangle);
                            }
                        }
                    }
                }
            }
        }

    }

    return rectangles;
}


PNM* HoughRectangles::transform()
{
    int width  = image->width(),
        height = image->height();

    CornerHarris cornerHarris(image);
    cornerHarris.setParameter("threshold", 30000000);
    cornerHarris.setParameter("sigma", 1.0);
    cornerHarris.setParameter("sigma_weight", 0.76);
    cornerHarris.setParameter("k_param", 0.05);
    PNM* newImage = cornerHarris.transform();

    std::vector<QPair<int,int>> foundPoints;

    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {

            QColor color = QColor::fromRgb(newImage->pixel(i,j));

            if (color.black() == 0) {
                QPair<int, int> pair = QPair<int, int>(i, j);
                foundPoints.push_back(pair);
            }
        }
    }

    for (unsigned i = 0; i < foundPoints.size(); i++) {
        std::vector<std::vector<QPair<int, int>>> rectangles = get_rectagles(foundPoints.at(i), foundPoints, i);

        if (rectangles.size() > 0) {
            for (unsigned l = 0; l < rectangles.size(); l++) {
                std::vector<QPair<int, int>> rectangle = rectangles.at(l);

                int min_x = qMin(rectangle.at(0).first, qMin(rectangle.at(1).first, qMin(rectangle.at(2).first, rectangle.at(3).first)));
                int max_x = qMax(rectangle.at(0).first, qMax(rectangle.at(1).first, qMax(rectangle.at(2).first, rectangle.at(3).first)));
                int min_y = qMin(rectangle.at(0).second, qMin(rectangle.at(1).second, qMin(rectangle.at(2).second, rectangle.at(3).second)));
                int max_y = qMax(rectangle.at(0).second, qMax(rectangle.at(1).second, qMax(rectangle.at(2).second, rectangle.at(3).second)));

                for (int x = min_x; x < max_x; x++) {
                    for (int y = min_y; y < max_y; y++) {

                        newImage->setPixel(x, y, 1);
                    }
                }
            }
        }
    }

    return newImage;
}

