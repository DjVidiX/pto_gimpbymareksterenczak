#include "histogram_stretching.h"

#include "../histogram.h"

HistogramStretching::HistogramStretching(PNM* img) :
    Transformation(img)
{
}

HistogramStretching::HistogramStretching(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

int findMin(QHash<int, int> *channel) {
    for(int i = 0; i < PIXEL_VAL_MAX; i++) {
        if (channel->value(i) > 0) {
            return i;
        }
    }
    return 0;
}

int findMax(QHash<int, int> *channel) {
    for(int i = PIXEL_VAL_MAX; i>=0; i--) {
        if (channel->value(i) > 0) {
            return i;
        }
    }
    return 0;
}

PNM* HistogramStretching::transform()
{
    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    if (image->format() == QImage::Format_Indexed8) {

        int min = findMin(image->getHistogram()->get(Histogram::LChannel));
        int max = findMax(image->getHistogram()->get(Histogram::LChannel));

        for (int x=0; x<width; x++) {
            for (int y=0; y<height; y++) {

                QRgb pixel = image->pixel(x,y);
                int v = qGray(pixel);

                int color = 255 * (v - min) / (max - min);

                newImage->setPixel(x,y, color);
            }
        }

    } else if (image->format() == QImage::Format_RGB32) {

        int minR = findMin(image->getHistogram()->get(Histogram::RChannel));
        int minG = findMin(image->getHistogram()->get(Histogram::GChannel));
        int minB = findMin(image->getHistogram()->get(Histogram::BChannel));

        int maxR = findMax(image->getHistogram()->get(Histogram::RChannel));
        int maxG = findMax(image->getHistogram()->get(Histogram::GChannel));
        int maxB = findMax(image->getHistogram()->get(Histogram::BChannel));

        for (int x=0; x<width; x++) {
            for (int y=0; y<height; y++) {

                QRgb pixel = image->pixel(x,y);
                int r = qRed(pixel);
                int g = qGreen(pixel);
                int b = qBlue(pixel);

                int colorR = 255 * (r - minR) / (maxR - minR);
                int colorG = 255 * (g - minG) / (maxG - minG);
                int colorB = 255 * (b - minB) / (maxB - minB);

                newImage->setPixel(x,y, qRgb(colorR, colorG, colorB));
            }
        }
    }

    return newImage;
}
