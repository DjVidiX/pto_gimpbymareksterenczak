#include "convolution.h"

/** Overloaded constructor */
Convolution::Convolution(PNM* img) :
    Transformation(img)
{
}

Convolution::Convolution(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

/** Returns a convoluted form of the image */
PNM* Convolution::transform()
{
    return convolute(getMask(3, Normalize), RepeatEdge);
}

/** Returns a sizeXsize matrix with the center point equal 1.0 */
math::matrix<float> Convolution::getMask(int size, Mode mode = Normalize)
{
    math::matrix<float> mask(size, size);

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            mask(i, j) = 0;
        }
    }

    int middle = floor(size / 2);
    mask(middle, middle) = 1.0;

    return mask;
}

/** Does the convolution process for all pixels using the given mask. */
PNM* Convolution::convolute(math::matrix<float> mask, Mode mode = RepeatEdge)
{
    int width  = image->width(),
        height = image->height();

    int size = mask.rowno();

    PNM* newImage = new PNM(width, height, image->format());

    float weight = sum(mask);

    if (image->format() == QImage::Format_RGB32) {

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                math::matrix<float> windowR = getWindow(x,y,size,RChannel,mode);
                math::matrix<float> windowG = getWindow(x,y,size,GChannel,mode);
                math::matrix<float> windowB = getWindow(x,y,size,BChannel,mode);

                math::matrix<float> akumulatorR = join(windowR, reflection(mask));
                math::matrix<float> akumulatorG = join(windowG, reflection(mask));
                math::matrix<float> akumulatorB = join(windowB, reflection(mask));

                float sumR = sum(akumulatorR);
                float sumG = sum(akumulatorG);
                float sumB = sum(akumulatorB);

                if (weight > 0) {
                    sumR = sumR / weight;
                    sumG = sumG / weight;
                    sumB = sumB / weight;
                }

                int r = sumR > PIXEL_VAL_MAX ? PIXEL_VAL_MAX : (sumR < PIXEL_VAL_MIN ? PIXEL_VAL_MIN : sumR);
                int g = sumG > PIXEL_VAL_MAX ? PIXEL_VAL_MAX : (sumG < PIXEL_VAL_MIN ? PIXEL_VAL_MIN : sumG);
                int b = sumB > PIXEL_VAL_MAX ? PIXEL_VAL_MAX : (sumB < PIXEL_VAL_MIN ? PIXEL_VAL_MIN : sumB);

                newImage->setPixel(x,y, qRgb(r,g,b));
            }
        }

    } else {

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                math::matrix<float> windowL = getWindow(x,y,size,LChannel,mode);

                math::matrix<float> akumulatorL= join(windowL, reflection(mask));

                float sumL = sum(akumulatorL);

                if (weight > 0) {
                    sumL = sumL / weight;
                }

                int l = sumL > PIXEL_VAL_MAX ? PIXEL_VAL_MAX : (sumL < PIXEL_VAL_MIN ? PIXEL_VAL_MIN : sumL);

                newImage->setPixel(x,y, l);
            }
        }

    }

    return newImage;
}

/** Joins to matrices by multiplying the A[i,j] with B[i,j].
  * Warning! Both Matrices must be squares with the same size!
  */
const math::matrix<float> Convolution::join(math::matrix<float> A, math::matrix<float> B)
{
    int size = A.rowno();
    math::matrix<float> C(size, size);

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            C(i, j) = A(i, j) * B(i, j);
        }
    }

    return C;
}

/** Sums all of the matrixes elements */
const float Convolution::sum(const math::matrix<float> A)
{
    float sum = 0.0;
    int size = A.rowno();

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            sum += A(i, j);
        }
    }

    return sum;

}


/** Returns reflected version of a matrix */
const math::matrix<float> Convolution::reflection(const math::matrix<float> A)
{
    int size = A.rowno();
    math::matrix<float> C(size, size);

    int x = size - 1;

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            C[x-i][x-j] = A[i][j];
        }
    }

    return C;
}
