#include "edge_gradient.h"
#include <QtCore/qmath.h>

EdgeGradient::EdgeGradient(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

EdgeGradient::EdgeGradient(PNM* img) :
    Convolution(img)
{
}

PNM* EdgeGradient::verticalDetection()
{
    return convolute(g_y, RepeatEdge);
}

PNM* EdgeGradient::horizontalDetection()
{
    return convolute(g_x, RepeatEdge);
}

PNM* EdgeGradient::transform()
{
    PNM* newImage = new PNM(image->width(), image->height(), image->format());
    int width = image->width();
    int height = image->height();

    PNM* image_x = this->horizontalDetection();
    PNM* image_y = this->verticalDetection();

    if (image->format() == QImage::Format_Indexed8) {

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {

                int v_x = qGray(image_x->pixel(x,y));
                int v_y = qGray(image_y->pixel(x,y));

                int v = qSqrt(qPow(v_x, 2) + qPow(v_y, 2));

                newImage->setPixel(x, y, v);
            }
        }

    } else if (image->format() == QImage::Format_RGB32) {

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {

                QRgb pixel_x = image_x->pixel(x,y);
                int r_x = qRed(pixel_x);
                int gr_x = qGreen(pixel_x);
                int b_x = qBlue(pixel_x);

                QRgb pixel_y = image_y->pixel(x,y);
                int r_y = qRed(pixel_y);
                int gr_y = qGreen(pixel_y);
                int b_y = qBlue(pixel_y);

                int r = qSqrt(qPow(r_x, 2) + qPow(r_y, 2));
                int g = qSqrt(qPow(gr_x, 2) + qPow(gr_y, 2));
                int b = qSqrt(qPow(b_x, 2) + qPow(b_y, 2));

                newImage->setPixel(x, y, qRgb(r, g, b));
            }
        }
    }

    return newImage;
}
