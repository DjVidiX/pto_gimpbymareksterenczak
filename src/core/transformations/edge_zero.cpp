#include "edge_zero.h"

#include "edge_laplacian_of_gauss.h"

EdgeZeroCrossing::EdgeZeroCrossing(PNM* img) :
    Convolution(img)
{
}

EdgeZeroCrossing::EdgeZeroCrossing(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* EdgeZeroCrossing::transform()
{
    int width = image->width(),
        height = image->height();

    int    size  = getParameter("size").toInt();
    double sigma = getParameter("sigma").toDouble();
    int    t     = getParameter("threshold").toInt();

    PNM* newImage = new PNM(width, height, QImage::Format_Indexed8);


    EdgeLaplaceOfGauss edgeLaplaceOfGauss(image);

    int v_0 = 128;

    for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {

            math::matrix<float> window = edgeLaplaceOfGauss.getWindow(x, y, size, LChannel, RepeatEdge);
            int window_size = window.colno();

            int max = window.max();
            int min = window.min();

            if (min < (v_0 - t) && max > (v_0 + t)) {
                int value = window(window_size/2, window_size/2);
                newImage->setPixel(x, y, value);
            } else {
                newImage->setPixel(x, y, 0);
            }

        }
    }



    return newImage;
}

