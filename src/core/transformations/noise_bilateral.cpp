#include "noise_bilateral.h"

NoiseBilateral::NoiseBilateral(PNM* img) :
    Convolution(img)
{
}

NoiseBilateral::NoiseBilateral(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* NoiseBilateral::transform()
{
    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    sigma_d = getParameter("sigma_d").toInt();
    sigma_r = getParameter("sigma_r").toInt();
    radius = sigma_d;

    if(image->format() == QImage::Format_Indexed8){

        for(int x = 0; x < width; x++) {
           for(int y  = 0; y < height; y++) {
                newImage -> setPixel(x, y, this->calcVal(x, y, LChannel));
            }
        }

    } else {

        for(int x = 0; x < width; x++) {
            for(int y = 0; y < height; y++) {

                int r = this->calcVal(x, y, RChannel);
                int g = this->calcVal(x, y, GChannel);
                int b = this->calcVal(x, y, BChannel);

                newImage -> setPixel(x, y, qRgb(r, g, b));
            }
        }
    }

    return newImage;
}

int NoiseBilateral::calcVal(int x, int y, Channel channel)
{
    int size = radius;

    math::matrix<float> window = getWindow(x, y, size, channel, RepeatEdge);

    float licznik = 0;
    float mianownik = 0;

    float windowMiddle = window(size/2, size/2);
    QPoint pointMiddle = QPoint(size/2, size/2);

    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++) {

            float iloczyn = colorCloseness(window(i, j), windowMiddle) * spatialCloseness(QPoint(i, j), pointMiddle);

            licznik += window(i, j) * iloczyn;
            mianownik += iloczyn;
        }
    }
    int value = licznik / mianownik;
    return value;

}

float NoiseBilateral::colorCloseness(int val1, int val2)
{
    float licznik = pow(val1 - val2, 2);
    float mianownik = 2 * pow(sigma_r, 2);

    return exp(- licznik / mianownik);
}

float NoiseBilateral::spatialCloseness(QPoint point1, QPoint point2)
{
    float part1 = pow(point1.x() - point2.x(), 2);
    float part2 = pow(point1.y() - point2.y(), 2);
    float licznik = part1 + part2;
    float mianownik = 2 * pow(sigma_d, 2);

    return exp(-licznik / mianownik);
}
