#include "bin_manual.h"

BinarizationManual::BinarizationManual(PNM* img) :
    Transformation(img)
{
}

BinarizationManual::BinarizationManual(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* BinarizationManual::transform()
{
    int threshold = getParameter("threshold").toInt();

    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, QImage::Format_Mono);

    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++)
        {
            QRgb color = image->pixel(x,y);

            int gray = qGray(color);

            newImage->setPixel(x,y, gray < threshold ? 0 : 1);
        }

    return newImage;
}




