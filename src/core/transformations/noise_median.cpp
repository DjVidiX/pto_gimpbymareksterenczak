#include "noise_median.h"

NoiseMedian::NoiseMedian(PNM* img) :
    Convolution(img)
{
}

NoiseMedian::NoiseMedian(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* NoiseMedian::transform()
{
    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    if (image->format() == QImage::Format_Indexed8) {

        for(int x = 0; x < width; x++) {
            for(int y = 0; y < height; y++) {
                newImage->setPixel(x, y, this->getMedian(x, y, LChannel));
            }
        }

    } else {

        for(int x = 0; x < width; x++) {
            for(int y = 0; y < height; y++) {

                int r = this->getMedian(x, y, RChannel);
                int g = this->getMedian(x, y, GChannel);
                int b = this->getMedian(x, y, BChannel);

                newImage->setPixel(x, y, qRgb(r, g, b));
            }
        }
    }

    return newImage;
}

int NoiseMedian::getMedian(int x, int y, Channel channel)
{
    int radius = getParameter("radius").toInt();

    int size = radius * 2  + 1;

    math::matrix<float> window = getWindow(x, y, size, channel, RepeatEdge);

    float* windowTable = new float[size * size];

    int counter = 0;
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++) {
            windowTable[counter++] = window(i, j);
        }
    }
    std::sort(windowTable, windowTable + size * size);

    // rozmiar zawsze jest nieparzysty
    int middle = (size*size)/2;
    int value = windowTable[middle];

    return value;
}
