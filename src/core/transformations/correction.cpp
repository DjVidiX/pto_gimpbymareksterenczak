#include "correction.h"
#include <QtCore/qmath.h>

Correction::Correction(PNM* img) :
    Transformation(img)
{
}

Correction::Correction(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* Correction::transform()
{
    float shift  = getParameter("shift").toFloat();
    float factor = getParameter("factor").toFloat();
    float gamma  = getParameter("gamma").toFloat();

    for (int i=0; i<=PIXEL_VAL_MAX; i++) {
        LUT[i]=i;

        int shiftValue = LUT[i]+shift;
        LUT[i]= shiftValue > PIXEL_VAL_MAX ? PIXEL_VAL_MAX : (shiftValue < PIXEL_VAL_MIN ? PIXEL_VAL_MIN : shiftValue);

        int factorValue = LUT[i]*factor;
        LUT[i]= factorValue > PIXEL_VAL_MAX ? PIXEL_VAL_MAX : (factorValue < PIXEL_VAL_MIN ? PIXEL_VAL_MIN : factorValue);

        int gammaValue = qPow(LUT[i], gamma);
        LUT[i]= gammaValue > PIXEL_VAL_MAX ? PIXEL_VAL_MAX : (gammaValue < PIXEL_VAL_MIN ? PIXEL_VAL_MIN : gammaValue);
    }

    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    if (image->format() == QImage::Format_Mono)
    {
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                QColor color = QColor::fromRgb(image->pixel(x,y)); // Getting the pixel(x,y) value

                newImage->setPixel(x,y, color == Qt::white ? LUT[PIXEL_VAL_MAX] : LUT[PIXEL_VAL_MIN]);
            }

    }
    else if (image->format() == QImage::Format_Indexed8)
    {
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                QRgb pixel = image->pixel(x,y); // Getting the pixel(x,y) value

                int v = qGray(pixel);    // Get the 0-255 value of the L channel
                newImage->setPixel(x,y, LUT[v]);
            }
    }
    else // if (image->format() == QImage::Format_RGB32)
    {

        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                QRgb pixel = image->pixel(x,y); // Getting the pixel(x,y) value

                int r = qRed(pixel);
                int g = qGreen(pixel);
                int b = qBlue(pixel);

                newImage->setPixel(x,y, qRgb(LUT[r], LUT[g], LUT[b]));
            }
    }

    return newImage;
}
