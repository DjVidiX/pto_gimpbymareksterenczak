#include "corner_harris.h"

#include "blur_gaussian.h"
#include "conversion_grayscale.h"
#include "edge_sobel.h"

CornerHarris::CornerHarris(PNM* img) :
    Convolution(img)
{
}

CornerHarris::CornerHarris(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* CornerHarris::transform()
{
    int    threshold    = getParameter("threshold").toInt();
    double sigma        = getParameter("sigma").toDouble(),
           sigma_weight = getParameter("sigma_weight").toDouble(),
           k_param      = getParameter("k").toDouble();

    int width  = image->width(),
        height = image->height();


    math::matrix<float> Ixx(width, height), Iyy(width, height), Ixy(width, height), cornerCandidates(width, height), cornerNonmaxSuppress(width, height);

    PNM* newImage = new PNM(width, height, QImage::Format_Mono);

    PNM* grayscaleImage = ConversionGrayscale(image).transform();


    BlurGaussian blurGaussian(grayscaleImage);
    blurGaussian.setParameter("size", 3);
    blurGaussian.setParameter("sigma", 1.6);
    PNM* blurGaussianImage = blurGaussian.transform();


    EdgeSobel edgeSobel(blurGaussianImage);
    math::matrix<float>* Gx = edgeSobel.rawHorizontalDetection();
    math::matrix<float>* Gy = edgeSobel.rawVerticalDetection();


    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            Ixx(i,j) = (*Gx)(i,j) * (*Gx)(i,j);
            Iyy(i,j) = (*Gy)(i,j) * (*Gy)(i,j);
            Ixy(i,j) = (*Gx)(i,j) * (*Gy)(i,j);
        }
    }

    for (int i = 1; i < width-1; i++) {
        for (int j = 1; j < height-1; j++) {

            float Sxx, Syy, Sxy, sumSxx = 0, sumSyy = 0, sumSxy = 0;

            for (int k = -1; k <= 1; k++) {
                for (int l = -1; l <= 1; l++) {
                    float blurGaussianGauss = BlurGaussian::getGauss(k,l,sigma);

                    sumSxx += Ixx(i+k, j+l) * blurGaussianGauss;
                    sumSyy += Iyy(i+k, j+l) * blurGaussianGauss;
                    sumSxy += Ixy(i+k, j+l) * blurGaussianGauss;
                }
            }

            Sxx = sumSxx / sigma_weight;
            Syy = sumSyy / sigma_weight;
            Sxy= sumSxy / sigma_weight;

            math::matrix<float>* H = new math::matrix<float>(2,2);
            (*H)(0,0) = Sxx;
            (*H)(1,0) = Sxy;
            (*H)(0,1) = Sxy;
            (*H)(1,1) = Syy;


            float r = H->det() - k_param * pow(H->trace(), 2);

            if (r > threshold) {
                cornerCandidates(i , j) = r;
            }
        }
    }

    bool search = true;

    while (search) {
        search = false;

        for (int i = 1; i < width-1; i++) {
            for (int j = 1; j < height-1; j++) {

                float max_value = 0;
                for (int k = i-1; k <= i+1; k++){
                    for (int l = j-1; l <= j+1; l++) {

                        if (k == i && l == j) {
                            continue;
                        }

                        if (cornerCandidates(k, l) > max_value) {
                            max_value = cornerCandidates(k, l);
                        }
                    }
                }

                if (cornerCandidates(i ,j) >= max_value) {
                    cornerNonmaxSuppress(i, j) = cornerCandidates(i ,j);

                } else {
                    if (cornerCandidates(i, j) > 0) {
                        search = true;
                    }
                    cornerNonmaxSuppress(i, j) = 0;
                }

            }
        }

        cornerCandidates = cornerNonmaxSuppress;
    }

    for (int i = 0; i < width-1; i++) {
        for (int j = 0; j < height-1; j++) {

            if (cornerCandidates(i, j) == 0 ) {
                newImage->setPixel(i,j, 0);

            } else {
                newImage->setPixel(i,j, 1);
            }
        }
    }

    return newImage;
}
