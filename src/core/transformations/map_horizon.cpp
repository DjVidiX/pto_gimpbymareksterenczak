#include "map_horizon.h"
#include "map_height.h"

MapHorizon::MapHorizon(PNM* img) :
    Transformation(img)
{
}

MapHorizon::MapHorizon(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* MapHorizon::transform()
{
    int width  = image->width(),
        height = image->height();

    double scale     = getParameter("scale").toDouble();
    int    sun_alpha = getParameter("alpha").toInt();
    int dx, dy;

    switch (getParameter("direction").toInt())
    {
    case NORTH: dx = 0; dy = -1; break;
    case SOUTH: dx = 0; dy = 1; break;
    case EAST:  dx = 1; dy = 0; break;
    case WEST:  dx = -1; dy = 0; break;
    default:
        qWarning() << "Unknown direction!";
        dx =  0;
        dy = -1;
    }

    PNM* newImage = new PNM(width, height, QImage::Format_Indexed8);

    PNM* mapHeightImage = MapHeight(image).transform();

    for (int x = 0; x < image->width(); x++) {
        for (int y = 0; y < image->height(); y++) {

            float a = 0;
            int current_h = qGray(mapHeightImage->pixel(x, y));

            for (int i = dx + x, j = dy + y; i < width && j < height && i >= PIXEL_VAL_MIN && j >= PIXEL_VAL_MIN; i += dx, j += dy) {

                int ray_h = qGray(mapHeightImage->pixel(i, j));

                if (current_h < ray_h) {

                    float dist = sqrt(pow(i - x, 2) + pow(j - y, 2)) * scale;

                    float ray_a = atan((ray_h - current_h) / dist);

                    if (ray_a > a) {
                        a = ray_a;
                    }
                }
            }
            float delta = a - sun_alpha * M_PI / 180;

            if (delta > 0) {

                newImage->setPixel(x, y, cos(delta) * PIXEL_VAL_MAX);

            } else{

                newImage->setPixel(x, y, PIXEL_VAL_MAX);
            }

        }
    }


    return newImage;
}
