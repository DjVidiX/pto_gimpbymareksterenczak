#include "map_height.h"
#include "conversion_grayscale.h"

MapHeight::MapHeight(PNM* img) :
    Transformation(img)
{
}

MapHeight::MapHeight(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* MapHeight::transform()
{
    ConversionGrayscale conversionGrayscale(image);

    return conversionGrayscale.transform();
}
