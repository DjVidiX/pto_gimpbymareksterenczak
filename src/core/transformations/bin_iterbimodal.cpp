#include "bin_iterbimodal.h"

#include "conversion_grayscale.h"
#include "histogram_equalization.h"
#include "../histogram.h"

BinarizationIterBimodal::BinarizationIterBimodal(PNM* img) :
    Transformation(img)
{
}

BinarizationIterBimodal::BinarizationIterBimodal(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* BinarizationIterBimodal::transform()
{
    int width = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, QImage::Format_Mono);

    HistogramEqualization* heq = new HistogramEqualization(image, supervisor);
    image = heq->transform();

    QHash<int, int> *histogram = image->getHistogram()->get(Histogram::LChannel);

    int T = 128, T_new = 0;

    while(true) {
        int licznik = 0, mianownik = 0, u_0 = 0, u_1 = 0;
        for (int i = 0; i < T; i++) {
            licznik += histogram->value(i) * i;
            mianownik += histogram->value(i);
        }
        u_0 = licznik / mianownik;
        licznik = 0;
        mianownik = 0;
        for (int i = T; i < PIXEL_VAL_MAX; i++) {
            licznik += histogram->value(i) * i;
            mianownik += histogram->value(i);
        }
        u_1 = licznik / mianownik;
        T_new = ( u_0 + u_1 ) / 2;

        if (T_new != T) {
            T = T_new;
        } else {
            break;
        }
    }

    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++)
        {
            QRgb color = image->pixel(x,y);
            int gray = qGray(color);

            newImage->setPixel(x,y, gray < T ? 0 : 1);
        }

    return newImage;
}



