#include "bin_gradient.h"

BinarizationGradient::BinarizationGradient(PNM* img) :
    Transformation(img)
{
}

BinarizationGradient::BinarizationGradient(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* BinarizationGradient::transform()
{
    int width = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, QImage::Format_Mono);

    int G_x = 0, G_y = 0;
    int licznik = 0;
    int mianownik = 0;


    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++)
        {
            G_x = qGray(getPixelRepeat(x+1, y)) - qGray(getPixelRepeat(x-1, y));
            G_y = qGray(getPixelRepeat(x, y+1)) - qGray(getPixelRepeat(x, y-1));

            licznik += qMax(G_x, G_y) * qGray(getPixelRepeat(x,y));
            mianownik += qMax(G_x, G_y);
        }

    int threshold = licznik / mianownik;

    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++)
        {
            QRgb color = image->pixel(x,y);
            int gray = qGray(color);

            newImage->setPixel(x,y, gray < threshold ? 0 : 1);
        }

    return newImage;
}


